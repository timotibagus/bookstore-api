# Book Store API

Book Store API is a backend app build with Node, Express, Sequelize, PostgreSQL
In this project i try to create Relationships between Book table & Store table to create Inventory table
I will use Sequelize Many-to-Many Association-Test CI

## API Endpoint

### GET

localhost:5000/books
localhost:5000/books/:id
localhost:5000/store
localhost:5000/inventory/:id

## POST

localhost:5000/books
localhost:5000/store/addbook

## Patch

localhost:5000/books/:id

# Delete

localhost:5000/books/:id
